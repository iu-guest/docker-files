#!/bin/sh
set -euC

for file in *.docker ; do
    name="$(basename "${file}" .docker)"
    docker build --file "${file}" --tag "${name}" "${name}"
done


